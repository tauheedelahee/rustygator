import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.2 as Controls

import org.kde.kirigami 2.8 as Kirigami
import org.kde.rustygator 1.0

Kirigami.ScrollablePage {
    title: "Feeds"

    contextualActions: [
        Kirigami.Action {
            text: "Add feed"
            onTriggered: {
                addSheet.edit = false
                addSheet.open()
            }
        }
    ]

    Kirigami.OverlaySheet {
        id: addSheet

        property bool edit: false
        contentItem: Kirigami.FormLayout {
            Controls.TextField {
                id: nameField
                Layout.fillWidth: true
                placeholderText: "Example Feed"
                Kirigami.FormData.label: "Name"
            }
            Controls.TextField {
                id: urlField
                Layout.fillWidth: true
                placeholderText: "https://example.org/feed.xml"
                Kirigami.FormData.label: "Url"
            }
            Controls.Button {
                text: addSheet.edit ? "Edit" : "Add"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                enabled: textField.text
                onClicked: {
                    if(addSheet.edit) {
                        addSheet.close()
                    } else {
                        feedsModel.add_feed(nameField.text, urlField.text)
                        addSheet.close()
                    }
                }
            }
        }
    }

    ListView {
        anchors.fill: parent

        model: FeedsModel {
            id: feedsModel
        }

        delegate: Kirigami.SwipeListItem {
            Controls.Label {
                width: parent.width
                text: model.display
                elide: Qt.ElideRight
            }
            width: parent.width
            height: Kirigami.Units.gridUnit * 2

            actions: [
                Kirigami.Action {
                    icon.name: "edit-entry"
                    text: "Edit"
                    onTriggered: {
                        addSheet.edit = true
                        addSheet.open()
                    }
                },
                Kirigami.Action {
                    icon.name: "list-remove"
                    text: "Remove"
                    onTriggered: feedsModel.remove_feed(currentIndex)
                }
            ]

            onClicked: pageStack.push("qrc:/qml/AtomPage.qml", {"url": model.url})
        }
    }
}
