import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.2 as Controls

import org.kde.kirigami 2.8 as Kirigami

import "qrc:/qml/"

Kirigami.ScrollablePage {
    title: modelData ? modelData.title : qsTr("Details")
    property QtObject modelData

    ColumnLayout {
        Image {
            width: 200
            source: modelData.logo
        }

        DetailsItem {
            name: "Title"
            content: modelData.title
        }

        DetailsItem {
            name: "Subtitle"
            content: modelData.subtitle
        }

        DetailsItem {
            name: "Last updated"
            content: modelData.updated
        }

        DetailsItem {
            name: "Generator"
            content: modelData.generator
        }

        DetailsItem {
            name: "Icon"
            content: modelData.icon
        }

        DetailsItem {
            name: "Categories"
            content: modelData.categories.join(", ")
        }
    }
}
