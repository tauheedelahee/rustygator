import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.2 as Controls

import org.kde.kirigami 2.8 as Kirigami

import "qrc:/qml/"

Kirigami.ScrollablePage {
    title: modelData ? modelData.title : qsTr("Details")
    property QtObject modelData

    ColumnLayout {
        DetailsItem {
            name: "Title"
            content: modelData.title
        }

        DetailsItem {
            name: "Last updated"
            content: modelData.updated
        }

        DetailsItem {
            name: "Authors"
            content: modelData.authors.join(", ")
        }

        DetailsItem {
            name: "Categories"
            content: modelData.categories.join(", ")
        }

        DetailsItem {
            name: "Contributor"
            content: modelData.contributors.join(", ")
        }

        DetailsItem {
            name: "Links"
            content: modelData.links.join(", ")
        }

        DetailsItem {
            name: "Published"
            content: modelData.published
        }

        DetailsItem {
            name: "Rights"
            content: modelData.rights
        }

        DetailsItem {
            name: "Source"
            content: modelData.source
        }

        DetailsItem {
            name: "Summary"
            content: modelData.summary
            textFormat: Text.RichText
        }

        DetailsItem {
            name: "Content"
            content: modelData.content
            textFormat: Text.RichText
        }
    }
}
