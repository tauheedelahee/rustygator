extern crate qmetaobject;

use crate::settings::{Settings, Feed};
use qmetaobject::*;
use std::collections::HashMap;
use num_traits::FromPrimitive;

//TODO: Save Settings path globally

#[derive(FromPrimitive, ToPrimitive)]
enum Roles {
    DisplayRole = USER_ROLE as isize  + 1,
    UrlRole
}

#[derive(QObject)]
pub struct FeedsModel {
    base: qt_base_class!(trait QAbstractListModel),
    settings: Settings,

    // methods
    add_feed: qt_method!(fn(&mut self, name: String, url: String)),
    remove_feed: qt_method!(fn(&mut self, index: usize))
}

impl Default for FeedsModel {
    fn default() -> Self {
        let mut path = dirs::home_dir().expect("Failed to find config path");
        path.push(".config/");
        path.push("rustygator_settings.json");
        let settings = match std::fs::read_to_string(path) {
            Ok(s) => serde_json::from_str(s.as_str()).expect("Settings are not valid json"),
            Err(e) => match e.kind() {
                std::io::ErrorKind::NotFound => Settings::default(),
                _ => panic!("Failed to read settings: {}", e)
            }
        };
        Self {
            base: QObjectCppWrapper::default(),
            settings: settings,
            add_feed: std::marker::PhantomData::<()>,
            remove_feed: std::marker::PhantomData::<()>
        }
    }
}

impl QAbstractListModel for FeedsModel {
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map: HashMap<i32, QByteArray> = HashMap::new();
        map.insert(Roles::DisplayRole as i32, "display".into());
        map.insert(Roles::UrlRole as i32, "url".into());
        return map;
    }

    fn row_count(&self) -> i32 {
        return self.settings.feeds.len() as i32;
    }

    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.row_count() as usize {
            return match FromPrimitive::from_i32(role) {
                Some(Roles::DisplayRole) => QString::from(self.settings.feeds[idx].name.clone()).into(),
                Some(Roles::UrlRole) => QString::from(self.settings.feeds[idx].url.clone()).into(),
                None => {
                    println!("role did not match anything");
                    QVariant::default()
                }
            }
        }

        return QVariant::default()
    }
}

impl FeedsModel {
    fn save(&self) {
        let serialized = serde_json::to_string_pretty(&self.settings).expect("Failed to serialize settings");
        let mut path = dirs::home_dir().expect("Failed to find config path");
        path.push(".config/");
        path.push("rustygator_settings.json");
        std::fs::write(path.to_str().unwrap(), serialized).expect("Failed to write settings");
    }

    fn add_feed(&mut self, name: String, url: String) {
        let end = self.settings.feeds.len();
        (self as &mut dyn QAbstractListModel).begin_insert_rows(end as i32, end as i32);
        self.settings.feeds.push(Feed {
            url: url,
            name: name
        });
        (self as &mut dyn QAbstractListModel).end_insert_rows();
        self.save();
    }

    fn remove_feed(&mut self, index: usize) {
        (self as &mut dyn QAbstractListModel).begin_remove_rows(index as i32, index as i32);
        self.settings.feeds.remove(index);
        (self as &mut dyn QAbstractListModel).end_remove_rows();
        self.save();
    }
}
